(define-module(modif)
  #:use-module (gnu)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix build-system trivial)
  #:use-module (gnu system)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu system mapped-devices)
  #:use-module (gnu system uuid)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system shadow)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xfce)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages video)
  #:use-module (gnu services)
  #:use-module (gnu services pm)
  #:use-module (gnu services virtualization)
  #:use-module (gnu services desktop)
  #:use-module (guix utils)
  #:use-module (gnu packages linux))

(define %powertop-service
  (simple-service 'powertop activation-service-type
		  #~(zero? (system* #$(file-append powertop "/sbin/powertop")
				    "--auto-tune"))))
(define %boot-logo-patch
  ;; Linux-Libre boot logo featuring Freedo and a gnu.
  (origin
    (method url-fetch)
    (uri (string-append "http://www.fsfla.org/svn/fsfla/software/linux-libre/"
                        "lemote/gnewsense/branches/3.16/100gnu+freedo.patch"))
    (sha256
     (base32
      "1hk9swxxc80bmn2zd2qr5ccrjrk28xkypwhl4z0qx4hbivj7qm06"))))

(define (linux-nonfree-urls version)
  "Return a list of URLs for Linux-Nonfree VERSION."
  (list (string-append
         "https://git.kernel.org/torvalds/t/"
         "linux-" version ".tar.gz"))) ;;i should seperate mainline and stable download links

(define-public linux-nonfree
  (let* ((version "5.1")) 
    (package
      (inherit linux-libre)
      (name "linux-nonfree")
      (version version)
      (source (origin
                (method url-fetch)
                (uri (linux-nonfree-urls version))
                (sha256
                 (base32
                  "0mi2lppzyh9q6grr75apk0mhpvji4xp12my4dw86hgrw877ra7mb")) ;;guix download https://git.kernel.org/torvalds/t/linux-version.tar.gz
		(patches (list %boot-logo-patch))))
      (synopsis "Mainline Linux kernel, nonfree binary blobs included.")
      (description "Linux is a kernel.")
      (license license:gpl2)
      (home-page "http://kernel.org/"))))

;;; Forgive me Stallman for I have sinned.

(define-public linux-firmware-nonfree
  (package
    (name "linux-firmware-non-free")
    (version "65b1c68c63f974d72610db38dfae49861117cae2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
                    (commit version)))
              (sha256
               (base32
                "1anr7fblxfcrfrrgq98kzy64yrwygc2wdgi47skdmjxhi3wbrvxz"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder (begin
                   (use-modules (guix build utils))
                   (let ((source (assoc-ref %build-inputs "source"))
                         (fw-dir (string-append %output "/lib/firmware/")))
                     (mkdir-p fw-dir)
                     (copy-recursively source fw-dir)
                     #t))))
    (home-page "")
    (synopsis "Non-free firmware for Linux")
    (description "Non-free firmware for Linux")
    ;; FIXME: What license?
    (license (license:non-copyleft "http://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git;a=blob_plain;f=LICENCE.radeon_firmware;hb=HEAD"))))

(operating-system
  (host-name "GNUIXUIX")
  (timezone "Europe/Berlin")
  (locale "en_US.utf8")
  (kernel linux-nonfree)
  (initrd-modules (cons*  %base-initrd-modules))
  (firmware
    (cons* linux-firmware-nonfree %base-firmware))
  (bootloader (bootloader-configuration
                (bootloader grub-bootloader)
                (target "/dev/sdb")))

  (mapped-devices
   (list (mapped-device
          (source (uuid "77aa7a34-c11c-4429-8d94-2f34d6af87d4"))
          (target "root")
          (type luks-device-mapping)))) ;;i still need to type the password twice...

  (file-systems (cons* (file-system
                        (device (file-system-label "root"))
                        (mount-point "/")
                        (type "ext4")
                        (dependencies mapped-devices))
		      (file-system
                        (device (file-system-label "boot"))
                        (mount-point "/boot")
                        (type "ext4")
                        (dependencies mapped-devices))
                      %base-file-systems))

  (users (cons (user-account
                (name "lars")
                (comment "Lars")
                (group "users")
                (supplementary-groups '("wheel" "netdev" "kvm" "libvirt" "lp"
                                        "audio" "video"))
                (home-directory "/home/lars")
		(shell #~(string-append #$zsh "/bin/zsh")))
               %base-user-accounts))
  (swap-devices
	(list "/swapfile"))
  ;; This is where we specify system-wide packages.
  (packages (cons* nss-certs         ;for HTTPS access
                   gvfs              ;for user mounts
		   zsh
		   pulseaudio
		   xrandr
		   libvdpau-va-gl
		   xfce4-whiskermenu-plugin
		   xfce4-pulseaudio-plugin
		   xfce4-power-manager
		   xfce4-clipman-plugin
		   xfce4-battery-plugin ;;i want plasma pls
		   flatpak ;;doesnt seem to work
                   %base-packages))
  (services
   (cons*
    (service tlp-service-type
	     (tlp-configuration
	       (tlp-default-mode "BAT")))
    (service libvirt-service-type
	     (libvirt-configuration
	       (unix-sock-group "libvirt"))) ;; can i edit /etc/libvirt/qemu.conf yet? also do i still need the modded libvirt for usb passthrough?
    (service virtlog-service-type)
    (service thermald-service-type)
    (bluetooth-service)
    %powertop-service
    (simple-service 'store-my-config
		    etc-service-type
		    `(("current-config.scm"
		       ,(local-file (assoc-ref
				      (current-source-location)
				      'filename)))))

  (xfce-desktop-service)
  (modify-services %desktop-services
		   (network-manager-service-type config =>
						 (network-manager-configuration
						   (inherit config)
						   (vpn-plugins (list
								  network-manager-openvpn)))) ;; does this work now?
		   (guix-service-type
		     config => (guix-configuration
				 (inherit config)
				 (extra-options '("--max-jobs=4"))
				 (substitute-urls
				   (cons* "https://berlin.guixsd.org" ;; can i specify the keys somewhere? i probably can. 
					  "https://mirror.hydra.gnu.org"
					  "https://mirror.guixsd.org"
					  "https://bayfront.guixsd.org"
					  %default-substitute-urls)))))))

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
